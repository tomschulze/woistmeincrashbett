#!/bin/sh
template=index.template.html
name=$(cat ./wer)

if [ -z "$name" ]; then
  name="tom"
fi

mkdir -p public && cp $template ./public/index.html && sed -i s/{{NAME}}/$name/g public/index.html

if [ $? != 0 ]; then
  rm -rf public && exit 1
fi
